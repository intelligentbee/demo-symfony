<?php

namespace  Application\Sonata\UserBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class WidgetParamAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('widgetType', 'entity', array('class' => 'IBW\Bundle\BizdevBundle\Entity\WidgetType'))
            ->add('name')
            ->add('paramType', null, array(
                'required' => false,
            ))
            ->add('isRequired', null, array(
                'required' => false,
            ))
            ->add('description', null, array(
                'required' => false,
            ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('widgetType')
            ->add('name')
            ->add('isRequired')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('widgetType')
            ->add('isRequired')
            ->add('description')
        ;
    }
}