set   :application,       "---------------------"
set   :deploy_to,         "---------------------"

# staging
set   :domain,            "------------" # staging - ibw2
set   :branch,			  "master"

# production
# set   :domain,          "-------------" # production - ibw3
# set   :branch,		  "master"

set   :scm,               :git
set   :repository,        "------------------------"

role  :web,               domain
role  :app,               domain
role  :db,                domain, :primary => true

set   :use_sudo,          false
set   :keep_releases,     3

set   :deploy_via,        :remote_cache

set   :shared_files,      ["app/config/parameters.yml"]
set   :shared_children,   [app_path + "/logs", web_path + "/uploads", "vendor"]
set   :use_composer,      true
set   :update_vendors,    false

ssh_options[:forward_agent] = true

after "deploy",           "deploy:set_perms_cache_logs"

namespace :deploy do
  task :set_perms_cache_logs, :roles => :app do
    run "sudo chmod -R 777 /home/jedibee/bizdev/current/app/cache/"
    run "sudo chmod -R 777 /home/jedibee/bizdev/current/app/logs/"
  end
end