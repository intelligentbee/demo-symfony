<?php

namespace IBW\Bundle\BizdevBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\Exception;

class ReportsController extends Controller
{
    public function indexAction(Request $request)
    {
        $arrReport = array();
        $arrReportPosition = array();
        $taxProfitPercent = 16;
        // getting sales (total products sold)
        // get all productive employees
        $repoCurrency = $this->getDoctrine()->getRepository('IBWBizdevBundle:Currency');
        $repoCost = $this->getDoctrine()->getRepository('IBWBizdevBundle:Cost');

        $fixedCostPerHour = $repoCost->getFixedCostPerHour();
        $defaultCurrency = $repoCurrency->findOneByIsDefault(true);

        $arrIncomePerEmployee = $this->get('bizdev_api_caller')->call('getProfitMargin');

        foreach ($arrIncomePerEmployee['detailed'] as $result)
        {
            /**
             * Departments
             */
            // sales - capacity
            if (isset($arrReport[$result['department_id']]['sales']))
            {
                $arrReport[$result['department_id']]['sales'] += $result['sales'];
            } else
            {
                $arrReport[$result['department_id']]['sales'] = $result['sales'];
            }

            // income
            if (isset($arrReport[$result['department_id']]['income']))
            {
                $arrReport[$result['department_id']]['income'] += $result['income_month'];
            } else
            {
                $arrReport[$result['department_id']]['income'] = $result['income_month'];
            }

            // costs
            $costForEmployee = 169 * $fixedCostPerHour['totalCost'] + $result['salary_cost'];
            if (isset($arrReport[$result['department_id']]['costs']))
            {
                $arrReport[$result['department_id']]['costs'] += $costForEmployee;
            } else
            {
                $arrReport[$result['department_id']]['costs'] = $costForEmployee;
            }

            /**
             * Positions
             */
            // sales - capacity
            if (isset($arrReportPosition[$result['position_id']]['sales']))
            {
                $arrReportPosition[$result['position_id']]['sales'] += $result['sales'];
            } else
            {
                $arrReportPosition[$result['position_id']]['sales'] = $result['sales'];
            }

            // income
            if (isset($arrReportPosition[$result['position_id']]['income']))
            {
                $arrReportPosition[$result['position_id']]['income'] += $result['income_month'];
            } else
            {
                $arrReportPosition[$result['position_id']]['income'] = $result['income_month'];
            }

            // costs
            if (isset($arrReportPosition[$result['position_id']]['costs']))
            {
                $arrReportPosition[$result['position_id']]['costs'] += $costForEmployee;
            } else
            {
                $arrReportPosition[$result['position_id']]['costs'] = $costForEmployee;
            }

            $arrReport[$result['department_id']]['department_name'] = $result['department_name'];
            $arrReport[$result['department_id']]['profit'] = $arrReport[$result['department_id']]['income'] -
                $arrReport[$result['department_id']]['costs'];
            $arrReport[$result['department_id']]['profit_net'] = $arrReport[$result['department_id']]['profit'] *
                (100 - $taxProfitPercent) / 100;
            $arrReport[$result['department_id']]['marja_profit'] = $arrReport[$result['department_id']]['profit_net']
                / $arrReport[$result['department_id']]['income'];

            $arrReportPosition[$result['position_id']]['position_name'] = $result['position_name'];
            $arrReportPosition[$result['position_id']]['profit'] =
                $arrReportPosition[$result['position_id']]['income'] -
                $arrReportPosition[$result['position_id']]['costs'];
            $arrReportPosition[$result['position_id']]['profit_net'] =
                $arrReportPosition[$result['position_id']]['profit'] * (100 - $taxProfitPercent) / 100;
            $arrReportPosition[$result['position_id']]['marja_profit'] =
                $arrReportPosition[$result['position_id']]['profit_net'] /
                $arrReportPosition[$result['position_id']]['income'];

        }

        return $this->render('IBWBizdevBundle:Reports:index.html.twig', array(
            'arrReport' => $arrReport,
            'arrReportPosition' => $arrReportPosition,
            'totalSales' => $arrIncomePerEmployee['totals']['totalSales'],
            'totalIncome' => $arrIncomePerEmployee['totals']['totalIncome'],
            'totalCosts' => $arrIncomePerEmployee['totals']['totalCosts'],
            'totalProfit' => $arrIncomePerEmployee['totals']['totalProfit'],
            'totalProfitNet' => $arrIncomePerEmployee['totals']['totalProfitNet'],
            'totalMarjaProfit' => $arrIncomePerEmployee['totals']['totalMarjaProfit'],
            'defaultCurrency' => $defaultCurrency,
            'arrIncomePerEmployee' => $arrIncomePerEmployee['detailed'],
            'fixedCostPerHour' => $fixedCostPerHour['totalCost'],
            'taxProfitPercent' => $taxProfitPercent,
        ));
    }

}
