<?php

namespace IBW\Bundle\BizdevBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Department
 */
class Department
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Department
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Department
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $positions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $employees;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->positions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->employees = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add positions
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Position $positions
     * @return Department
     */
    public function addPosition(\IBW\Bundle\BizdevBundle\Entity\Position $positions)
    {
        $this->positions[] = $positions;
    
        return $this;
    }

    /**
     * Remove positions
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Position $positions
     */
    public function removePosition(\IBW\Bundle\BizdevBundle\Entity\Position $positions)
    {
        $this->positions->removeElement($positions);
    }

    /**
     * Get positions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Add employees
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Employee $employees
     * @return Department
     */
    public function addEmployee(\IBW\Bundle\BizdevBundle\Entity\Employee $employees)
    {
        $this->employees[] = $employees;
    
        return $this;
    }

    /**
     * Remove employees
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Employee $employees
     */
    public function removeEmployee(\IBW\Bundle\BizdevBundle\Entity\Employee $employees)
    {
        $this->employees->removeElement($employees);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmployees()
    {
        return $this->employees;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if (!$this->createdAt)
        {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtValue()
    {
        $this->setUpdatedAt(new \DateTime());
    }
    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Employee
     */
    private $manager;


    /**
     * Set manager
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Employee $manager
     * @return Department
     */
    public function setManager(\IBW\Bundle\BizdevBundle\Entity\Employee $manager = null)
    {
        $this->manager = $manager;
    
        return $this;
    }

    /**
     * Get manager
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Employee 
     */
    public function getManager()
    {
        return $this->manager;
    }

    public function __toString()
    {
        return $this->name;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $goals;


    /**
     * Add goals
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Goal $goals
     * @return Department
     */
    public function addGoal(\IBW\Bundle\BizdevBundle\Entity\Goal $goals)
    {
        $this->goals[] = $goals;
    
        return $this;
    }

    /**
     * Remove goals
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Goal $goals
     */
    public function removeGoal(\IBW\Bundle\BizdevBundle\Entity\Goal $goals)
    {
        $this->goals->removeElement($goals);
    }

    /**
     * Get goals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGoals()
    {
        return $this->goals;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $subdepartments;

    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Department
     */
    private $parentDepartment;


    /**
     * Add subdepartments
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Department $subdepartments
     * @return Department
     */
    public function addSubdepartment(\IBW\Bundle\BizdevBundle\Entity\Department $subdepartments)
    {
        $this->subdepartments[] = $subdepartments;
    
        return $this;
    }

    /**
     * Remove subdepartments
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Department $subdepartments
     */
    public function removeSubdepartment(\IBW\Bundle\BizdevBundle\Entity\Department $subdepartments)
    {
        $this->subdepartments->removeElement($subdepartments);
    }

    /**
     * Get subdepartments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubdepartments()
    {
        return $this->subdepartments;
    }

    /**
     * Set parentDepartment
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Department $parentDepartment
     * @return Department
     */
    public function setParentDepartment(\IBW\Bundle\BizdevBundle\Entity\Department $parentDepartment = null)
    {
        $this->parentDepartment = $parentDepartment;
    
        return $this;
    }

    /**
     * Get parentDepartment
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Department 
     */
    public function getParentDepartment()
    {
        return $this->parentDepartment;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $indicators;


    /**
     * Add indicators
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Indicator $indicators
     * @return Department
     */
    public function addIndicator(\IBW\Bundle\BizdevBundle\Entity\Indicator $indicators)
    {
        $this->indicators[] = $indicators;
    
        return $this;
    }

    /**
     * Remove indicators
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Indicator $indicators
     */
    public function removeIndicator(\IBW\Bundle\BizdevBundle\Entity\Indicator $indicators)
    {
        $this->indicators->removeElement($indicators);
    }

    /**
     * Get indicators
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIndicators()
    {
        return $this->indicators;
    }
}