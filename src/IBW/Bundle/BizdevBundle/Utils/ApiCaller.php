<?php
/**
 * Created by PhpStorm.
 * User: costi
 * Date: 1/2/14
 * Time: 2:20 PM
 */

namespace IBW\Bundle\BizdevBundle\Utils;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Request;


class ApiCaller
{
    protected $request;
    protected $kernel;

    public function __construct(HttpKernelInterface $kernel, Request $request)
    {
        $this->kernel  = $kernel;
        $this->request = $request;
    }

    /**
     * make an api call
     *
     * @param $apiAction string
     * @param $params mixed
     *
     * @throws \Symfony\Component\Config\Definition\Exception\Exception
     * @return mixed
     */
    public function call($apiAction, $params = array())
    {
        $controller = 'IBWBizdevBundle:Api:' . $apiAction;
        $path = array_merge(
            array('_controller' => $controller),
            $params
        );
        $subRequest = $this->request->duplicate(array(), null, $path);

        $arrApiResponse = $this->kernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
        $arrApiResponse = json_decode($arrApiResponse->getContent(), true);

        if ($arrApiResponse['status'] != 'success')
        {
            throw new Exception($arrApiResponse['message']);
        }

        return $arrApiResponse['value'];
    }
} 