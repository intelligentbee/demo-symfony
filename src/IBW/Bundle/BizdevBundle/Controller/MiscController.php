<?php

namespace IBW\Bundle\BizdevBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MiscController extends Controller
{
    public function indexAction()
    {
    	return $this->render(
            'IBWBizdevBundle:Misc:index.html.twig'
        );
    }
}
