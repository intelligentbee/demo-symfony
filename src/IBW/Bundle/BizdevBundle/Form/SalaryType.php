<?php

namespace IBW\Bundle\BizdevBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SalaryType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('net', null, array(
                'required' => true,
                ))
            ->add('brut', null, array(
                'required' => true,
                ))
            ->add('totalCost', null, array(
                'required' => true,
                ))
            ->add('currency', null, array(
                'required' => true,
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IBW\Bundle\BizdevBundle\Entity\Salary'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ibw_bundle_bizdevbundle_salary';
    }
}
