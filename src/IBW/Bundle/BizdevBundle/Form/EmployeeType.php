<?php

namespace IBW\Bundle\BizdevBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use IBW\Bundle\BizdevBundle\Form\SalaryType;

class EmployeeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('position', null, array(
                'required' => true,
                ))
            ->add('department')
            ->add('isProductive', null, array(
                'required' => false,
                ))
            ->add('capacity', null, array(
                'required' => true,
                ))
            ->add('isDisabled', null, array(
                'required' => false,
                ))
            ->add('salary', new SalaryType())
            ->add('user', null, array(
                'required' => false,
                'property' => 'fullNameAndEmail',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IBW\Bundle\BizdevBundle\Entity\Employee',
            'cascade_validation' => true,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ibw_bundle_bizdevbundle_employee';
    }
}
