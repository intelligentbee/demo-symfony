<?php

namespace IBW\Bundle\BizdevBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Salary
 */
class Salary
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $net;

    /**
     * @var float
     */
    private $brut;

    /**
     * @var float
     */
    private $totalCost;

    /**
     * @var integer
     */
    private $version;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set net
     *
     * @param float $net
     * @return Salary
     */
    public function setNet($net)
    {
        $this->net = $net;
    
        return $this;
    }

    /**
     * Get net
     *
     * @return float 
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * Set brut
     *
     * @param float $brut
     * @return Salary
     */
    public function setBrut($brut)
    {
        $this->brut = $brut;
    
        return $this;
    }

    /**
     * Get brut
     *
     * @return float 
     */
    public function getBrut()
    {
        return $this->brut;
    }

    /**
     * Set totalCost
     *
     * @param float $totalCost
     * @return Salary
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;
    
        return $this;
    }

    /**
     * Get totalCost
     *
     * @return float 
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set version
     *
     * @param integer $version
     * @return Salary
     */
    public function setVersion($version)
    {
        $this->version = $version;
    
        return $this;
    }

    /**
     * Get version
     *
     * @return integer 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Salary
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Salary
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Employee
     */
    private $employee;

    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Currency
     */
    private $currency;


    /**
     * Set employee
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Employee $employee
     * @return Salary
     */
    public function setEmployee(\IBW\Bundle\BizdevBundle\Entity\Employee $employee = null)
    {
        $this->employee = $employee;
    
        return $this;
    }

    /**
     * Get employee
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Employee 
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set currency
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Currency $currency
     * @return Salary
     */
    public function setCurrency(\IBW\Bundle\BizdevBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;
    
        return $this;
    }

    /**
     * Get currency
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Currency 
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if (!$this->createdAt)
        {
            $this->setCreatedAt(new \DateTime());
            $this->version = 1;
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtValue()
    {
        $this->setUpdatedAt(new \DateTime());
    }
}