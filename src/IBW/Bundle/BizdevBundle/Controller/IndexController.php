<?php

namespace IBW\Bundle\BizdevBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
    	return $this->render(
            'IBWBizdevBundle:Index:index.html.twig'
        );
    }

}
