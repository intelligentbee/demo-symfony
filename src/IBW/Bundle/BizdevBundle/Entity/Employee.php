<?php

namespace IBW\Bundle\BizdevBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Employee
 */
class Employee
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var boolean
     */
    private $hasTaxExempt;

    /**
     * @var boolean
     */
    private $isDisabled;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Employee
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Employee
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set hasTaxExempt
     *
     * @param boolean $hasTaxExempt
     * @return Employee
     */
    public function setHasTaxExempt($hasTaxExempt)
    {
        $this->hasTaxExempt = $hasTaxExempt;
    
        return $this;
    }

    /**
     * Get hasTaxExempt
     *
     * @return boolean 
     */
    public function getHasTaxExempt()
    {
        return $this->hasTaxExempt;
    }

    /**
     * Set isDisabled
     *
     * @param boolean $isDisabled
     * @return Employee
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;
    
        return $this;
    }

    /**
     * Get isDisabled
     *
     * @return boolean 
     */
    public function getIsDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Employee
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Employee
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        if (!$this->createdAt)
        {
            $this->setCreatedAt(new \DateTime());
        }
    }

    /**
     * @ORM\PrePersist
     */
    public function setUpdatedAtValue()
    {
        $this->setUpdatedAt(new \DateTime());
    }
    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Position
     */
    private $position;


    /**
     * Set position
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Position $position
     * @return Employee
     */
    public function setPosition(\IBW\Bundle\BizdevBundle\Entity\Position $position = null)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Position 
     */
    public function getPosition()
    {
        return $this->position;
    }
    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Salary
     */
    private $salary;


    /**
     * Set salary
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Salary $salary
     * @return Employee
     */
    public function setSalary(\IBW\Bundle\BizdevBundle\Entity\Salary $salary = null)
    {
        $this->salary = $salary;
    
        return $this;
    }

    /**
     * Get salary
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Salary 
     */
    public function getSalary()
    {
        return $this->salary;
    }

    public function __toString()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @var boolean
     */
    private $isProductive = 0;


    /**
     * Set isProductive
     *
     * @param boolean $isProductive
     * @return Employee
     */
    public function setIsProductive($isProductive)
    {
        $this->isProductive = $isProductive;
    
        return $this;
    }

    /**
     * Get isProductive
     *
     * @return boolean 
     */
    public function getIsProductive()
    {
        return $this->isProductive;
    }
    /**
     * @var integer
     */
    private $capacity;


    /**
     * Set capacity
     *
     * @param integer $capacity
     * @return Employee
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;
    
        return $this;
    }

    /**
     * Get capacity
     *
     * @return integer 
     */
    public function getCapacity()
    {
        return $this->capacity;
    }
    /**
     * @var \IBW\Bundle\BizdevBundle\Entity\Department
     */
    private $department;


    /**
     * Set department
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Department $department
     * @return Employee
     */
    public function setDepartment(\IBW\Bundle\BizdevBundle\Entity\Department $department = null)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return \IBW\Bundle\BizdevBundle\Entity\Department 
     */
    public function getDepartment()
    {
        return $this->department;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $departments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->departments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add departments
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Department $departments
     * @return Employee
     */
    public function addDepartment(\IBW\Bundle\BizdevBundle\Entity\Department $departments)
    {
        $this->departments[] = $departments;
    
        return $this;
    }

    /**
     * Remove departments
     *
     * @param \IBW\Bundle\BizdevBundle\Entity\Department $departments
     */
    public function removeDepartment(\IBW\Bundle\BizdevBundle\Entity\Department $departments)
    {
        $this->departments->removeElement($departments);
    }

    /**
     * Get departments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDepartments()
    {
        return $this->departments;
    }
    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Employee
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}