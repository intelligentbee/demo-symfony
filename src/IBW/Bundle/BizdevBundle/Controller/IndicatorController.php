<?php

namespace IBW\Bundle\BizdevBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use IBW\Bundle\BizdevBundle\Entity\Indicator;
use IBW\Bundle\BizdevBundle\Form\IndicatorType;

/**
 * Indicator controller.
 *
 */
class IndicatorController extends Controller
{

    /**
     * Lists all Indicator entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IBWBizdevBundle:Indicator')->findAll();

        return $this->render('IBWBizdevBundle:Indicator:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Indicator entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Indicator();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('indicator_show', array('id' => $entity->getId())));
        }

        return $this->render('IBWBizdevBundle:Indicator:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Indicator entity.
    *
    * @param Indicator $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Indicator $entity)
    {
        $form = $this->createForm(new IndicatorType(), $entity, array(
            'action' => $this->generateUrl('indicator_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Save & Next'));

        return $form;
    }

    /**
     * Displays a form to create a new Indicator entity.
     *
     */
    public function newAction()
    {
        $entity = new Indicator();
        $form   = $this->createCreateForm($entity);

        return $this->render('IBWBizdevBundle:Indicator:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Indicator entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IBWBizdevBundle:Indicator')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Indicator entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('IBWBizdevBundle:Indicator:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Indicator entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IBWBizdevBundle:Indicator')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Indicator entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('IBWBizdevBundle:Indicator:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Indicator entity.
    *
    * @param Indicator $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Indicator $entity)
    {
        $form = $this->createForm(new IndicatorType(), $entity, array(
            'action' => $this->generateUrl('indicator_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Save & Next'));

        return $form;
    }
    /**
     * Edits an existing Indicator entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IBWBizdevBundle:Indicator')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Indicator entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

//            return $this->redirect($this->generateUrl('indicator_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('indicator_widget_params', array('id' => $id)));
        }

        return $this->render('IBWBizdevBundle:Indicator:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Indicator entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IBWBizdevBundle:Indicator')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Indicator entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('indicator'));
    }

    /**
     * Creates a form to delete a Indicator entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('indicator_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * widget page in wizard
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repoIndicator = $em->getRepository('IBWBizdevBundle:Indicator');
        $entity = $repoIndicator->find($id);

        $widgetData = json_decode($entity->getWidgetData(), true);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Indicator entity.');
        }

        // create widget form
        $form = $this->createWidgetForm($entity, $widgetData);

        return $this->render('IBWBizdevBundle:Indicator:widget.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * widget save action
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetUpdateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $repoIndicator = $em->getRepository('IBWBizdevBundle:Indicator');
        $entity = $repoIndicator->find($id);
        $widgetData = json_decode($entity->getWidgetData(), true);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Indicator entity.');
        }

        $form = $this->createWidgetForm($entity, $widgetData);
        $form->bind($request);
        if ($form->isValid()) {
            $entity->setWidgetData(json_encode($form->getData()));
            $em->flush();

            return $this->redirect($this->generateUrl('indicator_show', array('id' => $id)));
        }

        return $this->render('IBWBizdevBundle:Indicator:widget.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * creates the widget dynamic form
     *
     * @return
     */
    public function createWidgetForm(Indicator $entity, array $widgetData)
    {
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Indicator entity.');
        }
        $em = $this->getDoctrine()->getManager();
        $repoWidgetParam = $em->getRepository('IBWBizdevBundle:WidgetParam');

        $formBuilder = $this->createFormBuilder()
            ->setAction($this->generateUrl('indicator_widget_update', array(
                'id' => $entity->getId(),
            )))
            ->setMethod('POST');
        $widgetParams = $repoWidgetParam->findBy(array(
            'widgetType' => $entity->getWidgetType(),
        ));
        foreach ($widgetParams as $widgetParam)
        {
            $formBuilder->add($widgetParam->getName(), null, array(
                'required' => $widgetParam->getIsRequired(),
                'attr' => array(
                    'help_text' => $widgetParam->getDescription(),
                ),
                'data' => isset($widgetData[$widgetParam->getName()]) ? $widgetData[$widgetParam->getName()] : null,
            ));
        }
        $formBuilder->add('submit', 'submit', array(
            'label' => 'Save',
        ));

        return $formBuilder->getForm();
    }
}
