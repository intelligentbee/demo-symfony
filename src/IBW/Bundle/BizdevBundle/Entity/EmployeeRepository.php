<?php

namespace IBW\Bundle\BizdevBundle\Entity;

use Doctrine\ORM\EntityRepository;
use IBW\Bundle\BizdevBundle\Entity\Cost;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * EmployeeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EmployeeRepository extends EntityRepository
{
	public function getAllUsingCost(Cost $cost)
	{
		$rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata('IBW\Bundle\BizdevBundle\Entity\Employee', 'e');

        $q = $this->getEntityManager()->createNativeQuery('
            select e.id from Employee e 
            inner join Position p on e.position_id = p.id 
            inner join PositionCosts pc on pc.position_id = p.id 
            where pc.cost_id = ?', $rsm);
        $q->setParameter(1, $cost->getId());

        return $q->getResult();
	}

    public function updateEmployeesDepartment($employees, $department)
    {
        $arrEmployeeIds = array();
        foreach ($employees as $employee) {
            $arrEmployeeIds[] = $employee->getId();
        }
        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->update('IBWBizdevBundle:Employee', 'e')
            ->set('e.department', ':department')
            ->where('e.id IN (:arrEmployeesIds)')
            ->setParameter('department', $department)
            ->setParameter('arrEmployeesIds', $arrEmployeeIds);
        $q = $qb->getQuery();

        return $q->execute();
    }

    /**
     * get income per employee, position, department
     *
     * @param null $employeeId
     * @param null $departmentId
     * @param null $positionId
     * @return array
     */
    public function getProfitMarginReportPerEmployee($employeeId = null, $departmentId = null, $positionId = null)
    {
        $em = $this->getEntityManager()
            ->createQuery('select
                e.id as employee_id,
                e.firstName as employee_fist_name,
                e.lastName as employee_last_name,
                p.id as position_id,
                p.name as position_name,
                d.id as department_id,
                d.name as department_name,
                e.capacity*11/12 as sales,
                pr.price,
                pr.price*c.value as price_default_currency,
                pr.price * c.value * e.capacity * 11/12 as income_month,
                s.totalCost as salary_cost
                from IBWBizdevBundle:Employee e
                inner join IBWBizdevBundle:Position p with IDENTITY(e.position) = p.id
                inner join IBWBizdevBundle:Department d with IDENTITY(p.department) = d.id
                inner join IBWBizdevBundle:ProductComponent pc with IDENTITY(pc.position) = p.id
                inner join IBWBizdevBundle:Product pr with IDENTITY(pc.product) = pr.id
                inner join IBWBizdevBundle:Currency c with IDENTITY(pr.currency) = c.id
                inner join IBWBizdevBundle:Salary s with IDENTITY(e.salary) = s.id
                where e.isProductive = :isProductive' .
                (isset($employeeId) ? ' and e.id = ' . $employeeId : '') .
                (isset($departmentId) ? ' and d.id = ' . $departmentId : '') .
                (isset($positionId) ? ' and p.id = ' . $positionId : '')
            )
            ->setParameter('isProductive', true);
        $result = $em->getResult();
        return $result;
    }
}
